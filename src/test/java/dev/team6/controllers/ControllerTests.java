package dev.team6.controllers;

import dev.team6.models.CalenderUser;
import dev.team6.models.SubTask;
import dev.team6.models.Task;
import dev.team6.services.SubTaskService;
import dev.team6.services.TaskService;
import dev.team6.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
class ControllerTests<mockMvc> {

    private MockMvc loginMvc;
    private MockMvc userMvc;
    private MockMvc taskMvc;
    private MockMvc subTaskMvc;

    @MockBean
    UserService userService;

    @MockBean
    SubTaskService subTaskService;

    @MockBean
    TaskService taskService;

    @Autowired
    UserController userController;

    @Autowired
    SubTaskController subTaskController;

    @Autowired
    TaskController taskController;

    @Autowired
    LoginController loginController;

    @BeforeEach
    public void setUpLogin(){
        this.loginMvc = MockMvcBuilders.standaloneSetup(loginController).build();
    }

    @BeforeEach
    public void setUpUser(){
        this.userMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @BeforeEach
    public void setUpTask(){
        this.taskMvc = MockMvcBuilders.standaloneSetup(taskController).build();
    }

    @BeforeEach
    public void setUpSubTask(){
        this.subTaskMvc = MockMvcBuilders.standaloneSetup(subTaskController).build();
    }

    @Test
    public void checkIfLoginIsSuccessful() throws Exception {
        CalenderUser user = new CalenderUser("user1","pass1");
        doReturn(user).when(userService).getUserByUsername("user1");

        this.loginMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("username=user1&password=pass1"))
                .andExpect(status().isOk());
    }

    @Test
    public void checkIfLoginIsFailed() throws Exception {
        CalenderUser user = new CalenderUser("user1","pass1");
        doReturn(user).when(userService).getUserByUsername("user1");

        this.loginMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("username=user1&password=pass2"))
                .andExpect(status().isUnauthorized());
    }


    @Test
    public void createUserShouldCreateNewUser() throws Exception {
        CalenderUser user = new CalenderUser("user1", "pass1");
        doReturn(user).when(userService).createNewUser(user);

        this.userMvc.perform(post("/registerUser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("username=user1&password=pass1"))
                .andExpect(status().isCreated());
    }

//    @Test
//    public void createTaskforUser() throws Exception {
//        Date date = new Date();
//        Task task = new Task(0,"This is a task", date, date, date, false);
//        List<Task> taskList = new ArrayList<>();
//        taskList.add(task);
//        CalenderUser user = new CalenderUser("user1", "pass1", taskList);
//        doReturn(task).when(taskService).createNewTask(task);
//
//        this.taskMvc.perform(post("/tasks")
//                .contentType(MediaType.APPLICATION_JSON)
//                .characterEncoding("utf-8")
//                .content("{\n" +
//                        "    \"task_info\": \"task4  for user 1\",\n" +
//                        "    \"taskDate\": \"2019-02-03 10:08:02\",\n" +
//                        "    \"startTime\": \"2019-02-03 10:08:02\",\n" +
//                        "    \"endTime\": \"2019-02-03 10:08:02\",\n" +
//                        "    \"completed\": false\n" +
//                        "}")
//                .param("id", "0"))
//                .andExpect(status().isCreated());
//    }

    @Test
    public void returnAllSubTasks() throws Exception {
        List<SubTask> subTasks = Arrays.asList(
                new SubTask(1, "subtask 1", true),
                new SubTask(2, "subtask 2", false),
                new SubTask(3, "subtask 3", true));

        doReturn(subTasks).when(subTaskService).getAllSubtask();

        this.subTaskMvc.perform(get("/SubTasks"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].info").value(hasItems("subtask 1", "subtask 2", "subtask 3")));
    }

    @Autowired
    GAPIInteractionController gapiInteractionController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUpSync(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(gapiInteractionController).build();
    }

    @Test
    public void shouldReturnDefaultMessage() throws Exception {

        this.mockMvc.perform(post("/sync")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[\n" +
                        "    {\n" +
                        "        \"id\": 2,\n" +
                        "        \"task_info\": \"task for user 1\",\n" +
                        "        \"taskDate\": \"2021-08-01\",\n" +
                        "        \"startTime\": \"2021-08-01 15:13:00\",\n" +
                        "        \"endTime\": \"2021-08-01 17:15:00\",\n" +
                        "        \"completed\": false,\n" +
                        "        \"sub_tasks\": []\n" +
                        "    }\n" +
                        "]"))
                .andExpect(status().isOk());
    }
}