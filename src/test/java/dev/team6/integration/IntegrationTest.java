package dev.team6.integration;

import dev.team6.models.CalenderUser;
import dev.team6.models.Task;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Calendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class IntegrationTest {

//    @Test
//    public void testShouldReturnCorrectItem(){
//        TestRestTemplate restTemplate = new TestRestTemplate();
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Authorization", "auth-token");
//        HttpEntity<CalenderUser> request = new HttpEntity<>(headers);
//        ResponseEntity<CalenderUser> response =  restTemplate.exchange(
//                "http://localhost:8082/registerUser/2",
//                HttpMethod.GET,
//                request,
//                CalenderUser.class);
//        CalenderUser actualBody = response.getBody();
//        List<Task> taskList = null;
//        CalenderUser expectedBody = new CalenderUser(2, "Rocky Raccoon", "The Beatles", taskList);
//        assertAll(
//                ()->assertEquals(expectedBody, actualBody),
//                ()->assertEquals(200, response.getStatusCodeValue())
//        );
//    }

//    @Test
//    public void testGetOneUnauthorizedReturns401(){
//        TestRestTemplate restTemplate = new TestRestTemplate();
//        ResponseEntity<CalenderUser> response =  restTemplate.getForEntity("http://localhost:8082/tasks/1", CalenderUser.class);
//        assertEquals(401, response.getStatusCodeValue());
//    }
}
