package dev.team6.controllers;

import dev.team6.models.SubTask;
import dev.team6.models.Task;
import dev.team6.services.SubTaskService;
import dev.team6.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/SubTasks")
public class SubTaskController {
    @Autowired
    private SubTaskService subTaskService;

    @Autowired
    private TaskService taskService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<SubTask>> returnAllSubTask(@RequestParam(value = "completed", required = false)Boolean completed){
        if(completed != null){
            return new ResponseEntity<>(subTaskService.getSubTaskByCompleted(completed), HttpStatus.OK);
        }
        return new ResponseEntity<>(subTaskService.getAllSubtask(), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<SubTask> createNewSubTask(@RequestBody SubTask subTask, @RequestParam(value = "id", required = true)Integer id){
        Task task = taskService.getTaskById(id);
        List<SubTask> sub_tasks = task.getSub_tasks();
        sub_tasks.add(subTask);
        subTaskService.createNewSubTask(subTask);
        return new ResponseEntity<>(subTask, HttpStatus.CREATED);
    }
}