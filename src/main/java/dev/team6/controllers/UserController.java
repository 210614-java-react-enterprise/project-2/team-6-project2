package dev.team6.controllers;

import dev.team6.models.CalenderUser;
import dev.team6.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/registerUser")
public class UserController {

    @Autowired
    private UserService userService;
    CalenderUser user;

    @PostMapping(consumes = "application/x-www-form-urlencoded")
    public ResponseEntity<String> registerUser(@RequestParam("username") String username,
                                        @RequestParam("password") String pass){
        System.out.println("username: " + username + " password: " + pass);
        user = new CalenderUser(username, pass);
        userService.createNewUser(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
