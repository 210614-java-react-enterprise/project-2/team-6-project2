package dev.team6.controllers;

import dev.team6.models.CalenderUser;
import dev.team6.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LoginController {

    @Autowired
    private UserService userService;

    @PostMapping(value="/login", consumes = "application/x-www-form-urlencoded")
    public ResponseEntity<CalenderUser> login(@RequestParam("username") String username,
                                        @RequestParam("password") String pass){
        // check username and password against db records
        // if credentials are present, return token
        CalenderUser user;
        user = userService.getUserByUsername(username);
        if(user == null){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        if(user.getUsername().equals(username) && user.getPass().equals(pass)){
            int id = user.getId();
            return ResponseEntity.ok().header("Authorization", username+"-auth-token")
                    .header("Access-Control-Expose-Headers", "Content-Type, Allow, Authorization")
                    .body(user);
        } else {  // if not, return 401
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
