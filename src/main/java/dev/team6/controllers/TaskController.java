package dev.team6.controllers;

import dev.team6.models.CalenderUser;
import dev.team6.models.Task;
import dev.team6.services.TaskService;
import dev.team6.services.UserService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private UserService userService;

    private CalenderUser user;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Task>> returnAllTasks(
//            @RequestParam(value = "taskDate",required = false) LocalDateTime taskDate,
//                                                     @RequestParam(value = "startTime", required = false)LocalDateTime startTime,
//                                                     @RequestParam(value = "endTime", required = false)LocalDateTime endtime,
//                                                     @RequestParam(value = "completed", required = false)Boolean completed,
                                                     @RequestParam(value="id", required = true)Integer user_id){
        user = userService.getUserById(user_id);
        List<Task> taskList = user.getTasks();
//        if(taskDate!=null) {
//            return new ResponseEntity<>(taskService.getTasksByDate(taskDate), HttpStatus.OK);
//        } else if (startTime!=null) {
//            return new ResponseEntity<>(taskService.getTasksByStartTime(startTime), HttpStatus.OK);
//        }else if(endtime != null){
//            return new ResponseEntity<>(taskService.getTasksByEndTime(endtime), HttpStatus.OK);
//        }else if(completed != null){
//            return new ResponseEntity<>(taskService.getTasksByCompleted(completed), HttpStatus.OK);
//        }
        return new ResponseEntity<>(taskList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> returnTaskById(@PathVariable("id")int id){
        return new ResponseEntity<>(taskService.getTaskById(id), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<Task> createNewTask(@RequestBody Task task, @RequestParam(value = "id", required = true)Integer id){
        user = userService.getUserById(id);
        List<Task> taskList = user.getTasks();
        taskList.add(task);
        taskService.createNewTask(task);
        return new ResponseEntity<>(task, HttpStatus.CREATED);
    }

    @PostMapping(value = "/updateTask", consumes = "application/json")
    public ResponseEntity<Task> updateTask(@RequestBody Task task, @RequestParam(value = "id", required = true)Integer id){
        user = userService.getUserById(id);
        List<Task> taskList = user.getTasks();
        Task temp = taskList.get(task.getId());
        temp = task;
        return new ResponseEntity<>(taskService.updateTask(temp), HttpStatus.ACCEPTED);
    }
}
