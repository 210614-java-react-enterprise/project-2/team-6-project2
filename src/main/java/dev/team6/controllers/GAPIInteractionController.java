package dev.team6.controllers;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import dev.team6.models.Task;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.*;

@RestController
@CrossOrigin
public class GAPIInteractionController
{
    private final String APPLICATION_NAME = "Google Calendar API Interaction";
    private final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    private final String TOKENS_DIRECTORY_PATH = "tokens";

    private final List<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/calendar","https://www.googleapis.com/auth/calendar.events");
    //private final String CREDENTIALS_FILE_PATH = "credentials.json";

    @PostMapping(value = "/sync", consumes = "application/json")
    public ResponseEntity<String> synchronizeCalendar(@RequestBody Task[] postTasks)
    {
        //Store conversion to Google Events
        ArrayList<Event> insertEvents = new ArrayList<>();

        for (Task task:postTasks)
        {
            //Convert Java's LocalDateTimes to Google's DateTime
            //This is a terrible little hack to make the LocalDateTime cooperate with Google's DateTime format
            ////////////////////////////////////////////////////////////////////////////
            //Date startDate = new Date();
            //Date endDate = new Date(startDate.getTime() + 3600000);
            Date startDate = task.getStartTime();
            Date endDate = task.getEndTime();

            DateTime start = new DateTime(startDate, TimeZone.getTimeZone("UTC"));
            DateTime end = new DateTime(endDate, TimeZone.getTimeZone("UTC"));
            ////////////////////////////////////////////////////////////////////////////


            Event anEvent =createInsertableTaskFromRaw(
                    //Set task name
                    "TASK: " + task.getId(),
                    //Set task description
                    task.getTask_info(),
                    //Set task location (not yet implemented; currently placeholder value)
                    "",
                    start,
                    end
            );

            System.out.println("=========");
            System.out.println("Start before assignment is: " + start.toString());
            System.out.println("Start after assignment is: " + anEvent.getStart().toString());
            System.out.println("=========");

            insertEvents.add(anEvent);
        }

        authorizedInteraction("sync","",insertEvents);

        //TODO: Remove null return value
        return null;
    }

    //Create an AUTHORIZED Credential object.
    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException, JSONException
    {
        //Manifest the credentials JSON
        JSONArray authUris = new JSONArray();
        authUris.put("https://developers.google.com/oauthplayground");

        JSONObject jCredOuter = new JSONObject();

        JSONObject jCredInner = new JSONObject();
        jCredInner.put("client_id",System.getenv("C_ID"));
        jCredInner.put("project_id","trasker");
        jCredInner.put("auth_uri","https://accounts.google.com/o/oauth2/auth");
        jCredInner.put("token_uri","https://oauth2.googleapis.com/token");
        jCredInner.put("auth_provider_x509_cert_url","https://www.googleapis.com/oauth2/v1/certs");
        jCredInner.put("client_secret",System.getenv("C_SECRET"));
        jCredInner.put("redirect_uris",authUris);

        jCredOuter.put("web", jCredInner);

        System.out.println("JSON is:" + jCredOuter.toString());

        //Make the actual credential file
        String jsonString = jCredOuter.toString();
        Path path = Paths.get("/credentials.json");
        Files.deleteIfExists(path);

        File credFile = new File("credentials.json");
        FileWriter writer = new FileWriter("credentials.json");
        writer.write(jsonString);
        writer.close();
        System.out.println("Wrote file...");
        System.out.println("File non-absolute path is " + credFile.getPath());
        System.out.println("File absolute path is " + credFile.getAbsolutePath());

        //credFile.getAbsolutePath();

        //InputStream in = GAPIInteractionController.class.getResourceAsStream("/credentials.json");
        //InputStream in = GAPIInteractionController.class.getResourceAsStream(credFile.getAbsolutePath());
        //GAPIInteractionController.class.getRe
        InputStream in = new FileInputStream(credFile);




        if (in==null)
            throw new FileNotFoundException("CREDENTIALS FILE NOT FOUND...");

        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        //Build the flow object and trigger the user AUTHORIZATION request
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT,
                JSON_FACTORY,
                clientSecrets,
                SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    private void authorizedInteraction(String action, String detail, ArrayList<Event> events)
    {
        //BUILD a new AUTHORIZED api client service
        final NetHttpTransport HTTP_TRANSPORT;
        try
        {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

            //Service object to facilitate interaction
            Calendar service = new Calendar.Builder(HTTP_TRANSPORT,JSON_FACTORY,getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            //Pass off the action to the appropriate handler method
            action = action.toLowerCase();
            switch(action)
            {
                case "sync":
                    sync(service, action, detail, events);
                    break;
            }
        } catch (GeneralSecurityException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            System.out.println("Could not authorize interaction request.");
            e.printStackTrace();
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    /*
    private void listNextX(Calendar service, String action, String detail)
    {
        try
        {


            final int COUNT = Integer.parseInt(detail);

            //List the next COUNT events from the primary calendar
            DateTime now = new DateTime(System.currentTimeMillis());
            Events events = service.events().list("primary")
                    .setMaxResults(COUNT).setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
            List<Event> items = events.getItems();
            if (items.isEmpty())
                System.out.println("No upcoming events found.");
            else
            {
                System.out.println("UPCOMING EVENTS:");
                for (Event event : items)
                {
                    DateTime start = event.getStart().getDateTime();
                    if (start == null)
                        start = event.getStart().getDate();
                    System.out.printf("%s (%s)\n", event.getSummary(),start);
                }
            }
        }
        catch (NumberFormatException | IOException e)
        {
            System.out.println("Invalid event seek count (detail)");
        }
    }
     */

    //Creates Google Calendar events that are ready for insertion
    /*
    Task requirements:
    1. A title for the task
    2. A start time for the task as a DateTime
    3. An end time for the task as a DateTime
    Optional parameters:
    1. A description for the task
    2. A location for the task
     */
    private Event createInsertableTaskFromRaw(String title, String description, String location, DateTime startTime, DateTime endTime)
    {
        Event task = new Event()
                .setSummary(title)
                .setLocation("No specified location.")
                .setDescription(description);
        //Create start and end times for the task
        DateTime taskStart = startTime;
        DateTime taskEnd = endTime;
        task.setStart(new EventDateTime().setDateTime(taskStart).setTimeZone("America/New_York"));
        task.setEnd(new EventDateTime().setDateTime(taskEnd).setTimeZone("America/New_York"));

        return task;
    }

    private void insertAllTasks(Calendar service, ArrayList<Event> events) throws IOException
    {
        for (Event event:events)
        {
            event = service.events().insert("primary",event).execute();
            System.out.println("Event added to calendar:\n" + event.getHtmlLink());
            System.out.println("Event information:");
            System.out.println("Start: " + event.getStart().toString());
        }
    }

    public void sync(Calendar service, String action, String detail, ArrayList<Event> events) throws IOException
    {
        insertAllTasks(service,events);
        System.out.println("Synchronization attempt complete.");
    }
}