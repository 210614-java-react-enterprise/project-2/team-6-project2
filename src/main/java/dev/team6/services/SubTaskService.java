package dev.team6.services;

import dev.team6.daos.SubTaskRepository;
import dev.team6.models.SubTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubTaskService {

    @Autowired
    private SubTaskRepository subTaskRepository;

    public List<SubTask> getAllSubtask(){
        return subTaskRepository.findAll();
    }
    public SubTask getSubTaskById(int id){
        return subTaskRepository.getOne(id);
    }
    public List<SubTask> getSubTaskByCompleted(boolean completed){
        return subTaskRepository.findByCompleted(completed);
    }

    public SubTask createNewSubTask(SubTask st){
        return subTaskRepository.save(st);
    }
}
