package dev.team6.services;

import dev.team6.daos.UserRepository;
import dev.team6.models.CalenderUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public CalenderUser getUserById(int id){
        return userRepository.getOne(id);
    }
    public CalenderUser getUserByUsername(String username){
        return userRepository.findByUsername(username);
    }
    public CalenderUser createNewUser(CalenderUser user){
        return userRepository.save(user);
    }
}
