package dev.team6.services;

import dev.team6.daos.TaskRepository;
import dev.team6.models.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public List<Task> getAllTasks(){
        return taskRepository.findAll();
    }
    public Task getTaskById(int id){
        return taskRepository.getOne(id);
    }
    public List<Task> getTasksByDate(LocalDateTime dateTime){
        return taskRepository.findByTaskDate(dateTime);
    }
    public List<Task> getTasksByStartTime(LocalDateTime dateTime){
        return taskRepository.findByStartTime(dateTime);
    }
    public List<Task> getTasksByEndTime(LocalDateTime dateTime){
        return taskRepository.findByEndTime(dateTime);
    }
    public List<Task> getTasksByCompleted(boolean completed){
        return taskRepository.findByCompleted(completed);
    }
    public Task createNewTask(Task t){
        return taskRepository.save(t);
    }
    public Task updateTask(Task t){
        return taskRepository.save(t);
    }
}
