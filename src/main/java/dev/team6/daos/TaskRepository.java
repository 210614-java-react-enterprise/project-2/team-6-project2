package dev.team6.daos;

import dev.team6.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    List<Task> findByTaskDate(LocalDateTime dateTime);
    List<Task> findByStartTime(LocalDateTime dateTime);
    List<Task> findByEndTime(LocalDateTime dateTime);
    List<Task> findByCompleted(boolean completed);
}
