package dev.team6.daos;

import dev.team6.models.SubTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubTaskRepository extends JpaRepository<SubTask, Integer> {
    List<SubTask> findByCompleted(boolean completed);
}
