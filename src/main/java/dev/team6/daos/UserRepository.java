package dev.team6.daos;

import dev.team6.models.CalenderUser;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface UserRepository extends JpaRepository<CalenderUser, Integer> {
    CalenderUser findByUsername(String name);
}
