package dev.team6.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Component
@Aspect
public class AuthAspect {
    private Logger logger = LogManager.getLogger(LoggingAspect.class);
    @Around("within(dev.team6.controllers.TaskController)")
    public ResponseEntity<?> authorizeRequest(ProceedingJoinPoint pjp) throws Throwable {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        String token = request.getHeader("Authorization");
        if(token==null){
            // return some unauthorized response
            logger.warn("No token received from request");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            // execute controller method normally
            logger.info("token received");
            return (ResponseEntity<?>) pjp.proceed();
        }
    }
}
