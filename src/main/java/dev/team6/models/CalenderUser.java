package dev.team6.models;

import com.sun.istack.NotNull;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Component
@Entity
public class CalenderUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(unique = true)
    @NotNull
    private String username;

    @NotNull
    private String pass;

    @OneToMany
    private List<Task> tasks;

    public CalenderUser() {
    }

    public CalenderUser(String username, String pass) {
        this.username = username;
        this.pass = pass;
    }

    public CalenderUser(String username, String pass, List<Task> tasks) {
        this.id = id;
        this.username = username;
        this.pass = pass;
        this.tasks = tasks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalenderUser that = (CalenderUser) o;
        return id == that.id && username.equals(that.username) && pass.equals(that.pass) && Objects.equals(tasks, that.tasks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, pass, tasks);
    }

    @Override
    public String toString() {
        return "calender_user{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", pass='" + pass + '\'' +
                ", tasks=" + tasks +
                '}';
    }
}
