package dev.team6.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Component
@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    private int id;
    private String task_info;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date taskDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    private boolean completed;

    @OneToMany
    private List<SubTask> SubTasks;

    public Task() {
    }

    public Task(String task_info, Date taskDate, Date startTime, Date endTime, boolean completed) {
        this.task_info = task_info;
        this.taskDate = taskDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.completed = completed;
    }

    public Task(int id, String task_info, Date taskDate, Date startTime, Date endTime, boolean completed) {
        this.id = id;
        this.task_info = task_info;
        this.taskDate = taskDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.completed = completed;
    }

    public Task(int id, String task_info, Date taskDate, Date startTime, Date endTime, boolean completed, List<SubTask> SubTasks) {
        this.id = id;
        this.task_info = task_info;
        this.taskDate = taskDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.completed = completed;
        this.SubTasks = SubTasks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTask_info() {
        return task_info;
    }

    public void setTask_info(String task_info) {
        this.task_info = task_info;
    }

    public Date getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(Date taskDate) {
        this.taskDate = taskDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public List<SubTask> getSub_tasks() {
        return SubTasks;
    }

    public void setSub_tasks(List<SubTask> SubTasks) {
        this.SubTasks = SubTasks;
    }

    @Override
    public String toString() {
        return "task{" +
                "id=" + id +
                ", task_info='" + task_info + '\'' +
                ", taskDate=" + taskDate +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", completed=" + completed +
                ", sub_tasks=" + SubTasks +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id && completed == task.completed && task_info.equals(task.task_info) && taskDate.equals(task.taskDate) && Objects.equals(startTime, task.startTime) && Objects.equals(endTime, task.endTime) && Objects.equals(SubTasks, task.SubTasks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, task_info, taskDate, startTime, endTime, completed, SubTasks);
    }
}
