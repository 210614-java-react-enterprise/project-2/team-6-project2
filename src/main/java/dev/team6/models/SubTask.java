package dev.team6.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;

@Component
@Entity
public class SubTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subtask_id")
    private int id;
    private String info;
    private boolean completed;

    public SubTask() {
    }

    public SubTask(int id, String info, boolean completed) {
        this.id = id;
        this.info = info;
        this.completed = completed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubTask subTask = (SubTask) o;
        return id == subTask.id && completed == subTask.completed && Objects.equals(info, subTask.info);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, info, completed);
    }

    @Override
    public String toString() {
        return "SubTask{" +
                "id=" + id +
                ", info='" + info + '\'' +
                ", completed=" + completed +
                '}';
    }
}
